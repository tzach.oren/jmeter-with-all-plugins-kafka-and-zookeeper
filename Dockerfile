FROM egaillardon/jmeter-plugins:latest

# install kafka

ENV KAFKA_HOME /usr/lib/kafka

RUN apk add --update bash zip curl java-gcj-compat && \
    curl http://apache.spd.co.il/kafka/2.5.0/kafka_2.12-2.5.0.tgz| tar -xzf - && \
    mv kafka_2.12-2.5.0 ${KAFKA_HOME} && \
    rm /var/cache/apk/* 
RUN mkdir /tmp/zookeeper
RUN mkdir /tmp/kafka-logs
ENV PATH ${KAFKA_HOME}:${PATH}
WORKDIR ${KAFKA_HOME}
COPY start.sh ${KAFKA_HOME}/start.sh
RUN chmod a+x ${KAFKA_HOME}/start.sh
COPY stop.sh ${KAFKA_HOME}/stop.sh
RUN chmod a+x ${KAFKA_HOME}/stop.sh
EXPOSE 9092
EXPOSE 2181

# install gradle 6.4
RUN mkdir /usr/lib/gradle /app
ENV GRADLE_VERSION 6.4
ENV GRADLE_HOME /usr/lib/gradle/gradle-${GRADLE_VERSION}
ENV PATH ${PATH}:${GRADLE_HOME}/bin
WORKDIR /usr/lib/gradle
RUN set -x \
  && curl -L -O https://services.gradle.org/distributions/gradle-${GRADLE_VERSION}-bin.zip \
  && unzip gradle-${GRADLE_VERSION}-bin.zip \
  && rm gradle-${GRADLE_VERSION}-bin.zip 
