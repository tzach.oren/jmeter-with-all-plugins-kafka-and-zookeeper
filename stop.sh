#!/bin/bash
echo Stopping kafka
/usr/lib/kafka/bin/kafka-server-stop.sh /usr/lib/kafka/config/server.properties >/dev/null 2>&1
echo Stopping zookeeper
/usr/lib/kafka/bin/zookeeper-server-stop.sh /usr/lib/kafka/config/zookeeper.properties >/dev/null 2>&1
