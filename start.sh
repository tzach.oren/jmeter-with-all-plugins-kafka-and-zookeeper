#!/bin/bash
echo Stating zookeeper
/usr/lib/kafka/bin/zookeeper-server-start.sh /usr/lib/kafka/config/zookeeper.properties >/dev/null 2>&1 &
echo Stating kafka
/usr/lib/kafka/bin/kafka-server-start.sh /usr/lib/kafka/config/server.properties >/dev/null 2>&1 &
